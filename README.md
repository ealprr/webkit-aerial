# Aerial wekkit screensaver

Inspired by Aerial Apple TV screensaver, this is a webkit screensaver that plays movies as screensaver.


## Installation

Clone the project, edit index.html to add the common path of your movies and your movie's names.
You may also want to change month's abbreviations.

```js
var path = ""; // Movie's path
var videos = [
             ]; // all movie's names

var months = new Array('jan.', 'fév.', 'mars', 'avr.', 'mai', 'juin', 'jui.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'); // Month's names
```

Next you have to copy/move or link files to the cinnamon-screensaver path :

```
sudo ln -s $(pwd)/webkit-aerial /usr/share/cinnamon-screensaver/screensavers/webkit@cinnamon.org/
```

Now, you should set the screensaver througt configuration gui.


## Licence

GPL3

